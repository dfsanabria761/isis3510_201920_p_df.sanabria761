package com.kusarapps.beta.attendanceapp

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Intent
import android.graphics.Color
import android.opengl.Visibility
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.*
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivityForResult
import org.jetbrains.anko.toast
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class MainActivity : AppCompatActivity() {

    var m_bluetoothAdapter: BluetoothAdapter? = null

    val REQUEST_ENABLE_BLUETOOTH = 1





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mContext = applicationContext
        setContentView(R.layout.activity_main)

        m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if(m_bluetoothAdapter==null){
            toast("Not supported on this device")
            return
        }

        validateBluetooth()

        buttonStopDiscovery.setOnClickListener { stopDiscovery() }
        buttonStopDiscovery.isClickable=false;
        buttonStopDiscovery.setBackgroundColor(Color.GRAY)

        buttonDiscovery.setOnClickListener{ keepDiscovery() }

        confirmationText.visibility = View.GONE
    }
    private fun validateBluetooth(){
        if(!m_bluetoothAdapter!!.isEnabled){
            val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BLUETOOTH)
        }
    }
    private fun stopDiscovery(){
        if(m_bluetoothAdapter!!.isEnabled){
            m_bluetoothAdapter!!.cancelDiscovery()
            buttonStopDiscovery.isClickable=false
            buttonStopDiscovery.setBackgroundColor(Color.GRAY)
        }
    }

    private fun keepDiscovery(){

        validateBluetooth()
        m_bluetoothAdapter!!.startDiscovery()

        buttonStopDiscovery.isClickable=true
        buttonStopDiscovery.setBackgroundColor(Color.RED)
        checkAttendance()
    }

    override fun onStop() {
        super.onStop()

        m_bluetoothAdapter!!.disable()



    }
    private fun checkAttendance(){
        val TAG = "TAG"
        // Instantiate the cache
        val cache = DiskBasedCache(cacheDir, 1024 * 1024) // 1MB cap

        // Set up the network to use HttpURLConnection as the HTTP client.
        val network = BasicNetwork(HurlStack())

        // Instantiate the RequestQueue with the cache and network. Start the queue.
        val requestQueue = RequestQueue(cache, network).apply {
            start()
        }


        val ldt = LocalDateTime.now()
        val date = ""+ ldt.dayOfMonth + "-"+ldt.minusMonths(1).monthValue+"-"+ldt.year
        Log.d("date", date)
        //val url= "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/5-7-2019/students/201217484%20"
        val url= "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/"+date+"/students/201217484%20"
        val jsonObjectRequest = JsonObjectRequest(Request.Method.GET, url, null,
                    Response.Listener { response ->
                        failureText.visibility= View.GONE
                        confirmationText.visibility = View.VISIBLE
                        confirmationText.setText(""+confirmationText.text+date)

                        stopDiscovery()
                        m_bluetoothAdapter!!.disable()
                        requestQueue.cancelAll(TAG)
                        requestQueue.stop()
                    },Response.ErrorListener { error ->
                        toast("ATTENDANCE NOT FOUND")
                    }
                )
        jsonObjectRequest.tag = TAG
        requestQueue.add(jsonObjectRequest)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode ==REQUEST_ENABLE_BLUETOOTH){
            if(resultCode== Activity.RESULT_OK){
                if(m_bluetoothAdapter!!.isEnabled){
                    toast("Bluetooth is enabled")
                }
            }
        }
    }
}
